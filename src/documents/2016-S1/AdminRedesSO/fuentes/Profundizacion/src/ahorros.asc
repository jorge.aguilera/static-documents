== Ahorro de costes

La virtualización de escritorios, independiente de la tecnología que se utilice, supone unos ahorros
importantes de cara a la empresa. Según se detalla en una demo de la Universidad de Sevilla (Experiencia piloto con Ulteo-EvaOS de Solutia.pdf)
podemos obtener ahorros en las siguientes áreas:

- energéticos, ahorrando más de 100 euros / año / usuario
- soporte de hardware, reduciéndose hasta 10 veces el número de incidencias
- infraestructura de servidores, reduciendo hasta 10 veces las necesidades de servidores
- costes de licencias, al ser libre y gratuito
- reutilización de equipos, al tener unos requisitos mínimos


