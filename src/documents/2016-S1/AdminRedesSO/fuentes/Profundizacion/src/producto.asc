== Ulteo

Ulteo se define como un Open Virtual Desktop, o escritorio virtual abierto, al proveer por una parte,
de una solución donde los usuarios pueden acceder a escritorios que no se encuentran en su estación 
de trabajo, y por otra ser de código abierto.

Ulteo provee al usuario tanto de un entorno donde ejecutar aplicaciones o de un escritorio completo,
alojado todo ello en un servidor Linux o Window, accesible mediante un portal web, o integrado en
el propio escritorio del usuario y ejecutandose como si estuvieran en este.

La experiencia del usuario es completa, en el sentido de que las aplicaciones pueden acceder al
sistema de ficheros local, acceder a las impresoras locales, dispositivos conectados al equipo, e 
incluso realizar funciones de copy and paste como cualquier aplicación local. 

De cara al administrador, este puede monitorizar y controlar a través de un interface web cualquier
sesión, configurar la carga entre máquinas, etc. 

=== Módulos

Ulteo como tal se divide en diferentes módulos, cada uno realizando una función específica

image::arquitectura-ulteo.png[]


==== Session Manager

Encargada de controlar las sesiones así como de alojar el interface para el administrador, es el core
de Ulteo.

==== Aplication Server

Es el lugar donde las aplicaciones se ejecutan. Si tenemos aplicaciones que requieren diferentes 
sistemas operativos (Windows y Linux) necesitaremos al menos un AS para cada tipo.

==== Client

Ejecutado en el equipo del usuario, se puede utilizar en modo aplicación o en modo escritorio. En el 
primer modo, el usuario ejecuta aplicaciones como si estuvieran en su equipo mientras que en el modo
escritorio, el sistema le presenta un entorno de trabajo completo pero remoto.

Existen diferentes clientes, como puede ser un cliente Web (aplicación Java), un cliente HTML5 e incluso
un cliente nativo


=== Características

Ulteo ofrece una serie de características que le diferencian de otros productos similares como pueden ser:

* Ofrece hosting para aplicaciones tanto Windows como Linux (en diferentes distros como Centos, Ubuntu, etc).
* Integración con el escritorio físico del usuario permitiendo una experiencia nativa desde un entorno remoto.
* Gestión integrada de qué puede aplicaciones puede ejecutar un usuario y cúales no.
* Transferir sesiones de un dispositivo a otro de tal forma que un usuario puede iniciar sesión en un PC y
continuar trabajando en la tablet, por ejemplo.
* Gestión de carpetas compartidas
* Diferentes tipos de acceso (web, nativo, móvil, ..)
* Funcionalidades de sistema como balanceador de carga, alertas, gestión de sesiones, encriptado SSL, uso de
cualquier sistema de directorios (Active Directory, LDAP, ..) 
* ...

Además de tener la opción Open Source que permite el despliegue de una infraestructura sin costes de licencias,
Ulteo cuenta con una versión comercial donde se incluye soporte técnico, acceso a foros, etc.

