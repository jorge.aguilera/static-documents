Static Documents
----------------

JBake es un generador de Blog estático al estilo de Hugo, Jekyll, Middleman, etc donde creas tus post en ficheros planos
y el sistema se encarga de generar el HTML correspondiente evitando que necesites bases de datos, lenguajes en el servidor
, y demás complejidades que al final encarecen la solución.

Mediante Static Documents puedes crear un site de documentos donde el usuario puede navegar a través de las carpetas
y acceder a sus ficheros de una forma cómoda. Simplemente coloca tus documentos en la carpeta _src/documents_ y el sistema
se encargará de generar tu site.

## Mi servidor web

Si quieres que el site se cree en tu equipo necesitarás tener instalado Java 1.7 o superior. Una vez que tengas la carpeta
_src/documents_ a tu gusto ejecuta *./gradlew build* y copia la carpeta *./build/public* a tu servidor

Si lo prefieres puedes comprimir todos tus ficheros en un zip y ubicarlo en _src/documents_ que el proceso lo descomprimirá
automáticamente. De esta manera es más fácil subir documentación al repositorio

## Gitlab pages

Si quieres utilizar la infraestructura de Gitlab necesitarás crear un proyecto con el nombre que tu quieras y clonar este
proyecto en el mismo. Una vez que tengas tus documentos a tu gusto en la carpeta _src/documents_ simplemente realiza un
*pull* y a los pocos segundos tendrás tu contenido publicado

## Github

Proximamente podrás publicar tu Static Documents en Github 

## Docker

Y porqué no crear una imagen Docker con tus documentos para que sean accesibles vía un contenedor httpd:2.4.
Si publicas tu imagen en Docker.io, Gitlab o similar cualquier usuario podrá navegar en local:

    docker run --rm -p 8081:80 jagedn/static-documents:latest 
    firefox localhost:8081 

Simplemente configura tu servidor (Docker.io, Gitlab,...) en la property dockerRegistry:

    ext{
        dockerRegistry='docker.io'
    }

    group 'tu_usuario_'


    ./gradlew pushDockerRegistry

## Docker con CI (Gitlab)

Si lo prefieres, puedes hacer que el proceso de Integracion Continua de Gitlab cree la imagen y la actualice en
tu espacio. Para ello simplemente tienes que etiquetar la version y decirselo a Gitlab:

    git tag -a X.X.X -m "The last Frontier"
    git push --tags

A los pocos minutos tu imagen se encontrará disponible en tu registry
 
## Blog

Y por supuesto puedes bloguear en tu proyecto usando JBake.


