import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class MixTask extends DefaultTask{

  String from
  String into

 def origen, destino, content

 @TaskAction
 def mix(){
	origen = new File(from)
	assert origen.exists()
	destino = new File(into)
	assert destino.exists()
	content = new File(into+'/content')
	assert content.exists()

	makeDirectoryTemplate(origen, content,0)
	origen.eachDir(){ subdir->
		makeDirectoryTemplate(subdir, content,1)
	}
 }

 void makeDirectoryTemplate(directory, destino, deep){
	   println "makeDirectoryTemplate $directory.absolutePath"

	   def f = new File(destino, "${directory.path-from}/${directory.name}.adoc")
	   f.parentFile.mkdirs()
	   f.withWriter{ w->

w.println """= ${directory.name}
:jbake-type: folder
:jbake-status: published
:deep: ${deep}
:icons: font
"""
		w.println "## Carpetas\n"
                directory.eachDir(){ dir->
                        w.println "* icon:folder[] link:++${dir.name}/${dir.name}.html++[$dir.name]\n"
                        makeDirectoryTemplate(dir, destino, deep+1)
                }
		w.println " "

		w.println "## Ficheros\n"

		directory.eachFile(groovy.io.FileType.FILES){ file->
			w.println "* icon:file[] link:++${file.name}++[${file.name} (${formatSize(file.size())})]\n"
		}
		w.println " "	
	   }
  }

  String formatSize(long v) {
	    if (v < 1024) return v + " B";
	    int z = (63 - Long.numberOfLeadingZeros(v)) / 10;
	    return String.format("%.1f %sB", (double)v / (1L << (z*10)), " KMGTPE".charAt(z));
  }
  
}
